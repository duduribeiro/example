class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.string :name
      t.string :media
      t.datetime :show_time

      t.timestamps
    end
    add_index :shows, :media
  end
end
