Agrid::Application.routes.draw do
  root "pages#index"
  resources :users, except: [:destroy, :edit, :update ] do
    resources :shows, only: [ :index ]
  end
  resources :shows
  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  delete "/logout", to: "sessions#destroy"
end
