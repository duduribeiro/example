# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Agrid::Application.config.secret_key_base = 'a0b29f64b580ffb886029b874620c65625501537b74d7321b6d003dd61f662803759e85409cc067cd493fb20ac03ddfbf38fb50745ca00a04088a1adf40b34c5'
