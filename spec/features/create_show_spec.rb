require 'rails_helper'

feature "Creating Shows" do
  let(:user){FactoryGirl.create(:user)}
  before do
    sign_in(user)
    visit root_path

    click_link I18n.t("menu.shows")

    click_link I18n.t("menu.show.new_show")
  end
  scenario "successfully create show" do
    show = FactoryGirl.build(:show)
    fill_in I18n.t("activerecord.attributes.show.name"), with: show.name
    select(I18n.t("activerecord.attributes.show.medias.#{show.media}") ,from: I18n.t("activerecord.attributes.show.media")  )    
    fill_in I18n.t("activerecord.attributes.show.show_time"), with: show.show_time
    click_button I18n.t("form.save_button")
    
    expect(Show.last.user_id).to eq user.id

    expect(page).to have_content( I18n.t("messages.show_created") )
  end
end