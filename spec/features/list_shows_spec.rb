require 'rails_helper'

feature "List Shows" do
  let(:user) { FactoryGirl.create(:user) }
  before do
    sign_in(user)
  end
  scenario "exib all shows" do
    next_shows = FactoryGirl.create_list(:show, 4, user: user )
    old_shows = FactoryGirl.create_list(:old_show, 5, user: user)
    visit shows_path
    shows = (next_shows << old_shows).flatten
    shows.each do |show|
      expect(page).to have_content(show.name)
    end
  end

  scenario "don't show other users shows" do
    next_shows = FactoryGirl.create_list(:show, 4, user: FactoryGirl.create(:user) )
    visit shows_path
    next_shows.each do |show|
      expect(page).to have_no_content(show.name)
    end
  end
end