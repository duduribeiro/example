require 'rails_helper'

feature "Login" do
  scenario "login user" do
    user = FactoryGirl.create(:user)
    visit login_path

    fill_in "login[email]", with: user.email
    fill_in "login[password]", with: user.password
    click_button I18n.t("views.sessions.form.login_button")
  end
end