require 'rails_helper'

feature "Home" do
  context "not logged in" do
    scenario "redirect to signin page when user not logged in" do
      visit root_path

      expect(current_path).to eq login_path
      expect(page).to have_content(I18n.t("views.sessions.new.login_title"))
    end
  end

  context "logged in" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      sign_in(user)
    end
    scenario "redirect to home page" do
      visit root_path

      expect(current_path).to eq current_path
      expect(page).to have_content(I18n.t("views.pages.index.title"))
    end

    scenario "instead show signup menu, show logout" do
      visit root_path

      expect(page).to have_content(I18n.t("menu.logout"))
      expect(page).to have_no_content(I18n.t("menu.signup"))
      expect(page).to have_no_content(I18n.t("menu.login"))
    end

    scenario "show today shows" do
      shows = FactoryGirl.create_list(:show, 4, show_time: DateTime.now, user: user)
      new_shows = FactoryGirl.create_list(:show, 3, user: user)

      visit root_path

      shows.each do |show|
        expect(page).to have_content(show.name)
      end
      new_shows.each do |show|
        expect(page).to have_no_content(show.name)
      end      
    end
  end
end
