require 'rails_helper'

feature "Logout" do
  context "logged in" do
    before do
      sign_in(FactoryGirl.create(:user))
    end
    scenario "logout" do
      visit root_path

      click_link I18n.t("menu.logout")

      expect(page).to have_content(I18n.t("messages.successfully_logout"))

      visit root_path

      expect(current_path).to eq login_path
    end
  end
end
