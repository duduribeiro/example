require 'rails_helper'

feature "List other users shows" do
  let(:all_users) { FactoryGirl.create_list(:user, 5) }
  let(:my_user) { all_users.shift }
  before do
    sign_in(my_user)
  end  
  scenario "don't list my user" do
    visit users_path
    expect(page).to have_no_content(my_user.name)
  end
  scenario "list other users" do
    visit users_path
    all_users.each do |user|
      expect(page).to have_content(user.name)
    end
  end  
end