require 'rails_helper'

feature "Show User Info" do
  before do
    sign_in(FactoryGirl.create(:user))
  end
  scenario "show all user information with a link to view shows" do
    user = FactoryGirl.create(:user)
    visit user_path(user)
    expect(page).to have_content( user.name )
    expect(page).to have_content( user.email )

    expect(page).to have_content( I18n.t("views.users.show.user_shows_link") )
  end

  @javascript
  scenario "list user shows" do
    user = FactoryGirl.create(:user)
    user_shows = FactoryGirl.create_list(:show, 4, user: user)
    visit user_path(user)
    click_link I18n.t("views.users.show.user_shows_link")

    user_shows.each do |show|
      expect(page).to have_content(show.name)
    end    
  end
end