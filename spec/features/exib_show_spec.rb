require 'rails_helper'

feature "Exib Shows" do
  let(:show) { FactoryGirl.create(:show) }
  before do
    sign_in(FactoryGirl.create(:user))
  end
  scenario "exib show infos" do
    visit show_path(show)
    expect(page).to have_content( I18n.t("views.shows.show.show_title", name: show.name) )
    expect(page).to have_content( I18n.t("views.shows.show.media_title", media: I18n.t("activerecord.attributes.show.medias.#{show.media}") ) )
  end
end