require 'rails_helper'

feature "Signing Up" do
  scenario "Successful sign up" do
    visit root_path

    click_link I18n.t("menu.signup")

    fill_in I18n.t("activerecord.attributes.user.name"), with: "Test"
    fill_in I18n.t("activerecord.attributes.user.email"), with: "user@user.com"
    fill_in I18n.t("activerecord.attributes.user.password"), with: "password"
    fill_in I18n.t("activerecord.attributes.user.password_confirmation"), with: "password"
    click_button I18n.t("views.users.form.signup_button")

    expect(page).to have_content(I18n.t("messages.successfully_signed_up"))
  end
end