require 'rails_helper'

RSpec.describe User, :type => :model do
  context "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should have_secure_password }
  end

  describe ".others" do
    it "not show my user" do
      users = FactoryGirl.create_list(:user, 3)
      others = User.others(users.first.id)
      expect(others.size).to eq 2
      expect(others.include?(users.first)).to be_falsey
    end
  end
end
