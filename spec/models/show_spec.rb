require 'rails_helper'

RSpec.describe Show, :type => :model do
  context "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:media) }
  end

  describe ".media_attributes_for_select" do 
    it "returns i18n text" do
    expected_values = [ I18n.t("activerecord.attributes.show.medias.movie"),
      I18n.t("activerecord.attributes.show.medias.series"),
      I18n.t("activerecord.attributes.show.medias.tv")
    ]
    values = Show.media_attributes_for_select.map(&:first).sort      
    expect(values).to eq expected_values
    end
  end

  describe ".to_watch" do
    it "returns only future shows to watch" do
      new_shows = FactoryGirl.create_list(:show, 5)
      old_shows = FactoryGirl.create_list(:old_show, 8)
      
      expect(Show.to_watch.size).to eq new_shows.size
    end
  end

  describe ".today_shows" do
    it "only list today shows" do
      today_shows = [ FactoryGirl.create(:show, show_time: DateTime.now), FactoryGirl.create(:show, show_time: DateTime.now) ]
      other_shows = FactoryGirl.create_list(:show, 3)
      expect(Show.today_shows.size).to eq today_shows.size
    end
  end
end
