FactoryGirl.define do
  factory :show do
    name { Faker::Name.name }
    media { Show.media.keys.sample }
    show_time { rand(1.years).seconds.from_now }
    user

    factory :old_show do
      show_time { rand(1.years).seconds.ago }
    end
  end
end