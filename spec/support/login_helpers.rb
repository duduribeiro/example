module LoginHelpers
  def sign_in(user)
    visit login_path
    fill_in "login[email]", with: user.email
    fill_in "login[password]", with: user.password
    click_button I18n.t("views.sessions.form.login_button")
    expect(page).to have_content(I18n.t("messages.successfully_login"))
  end
end

RSpec.configure do |c|
  c.include LoginHelpers, type: :feature
end