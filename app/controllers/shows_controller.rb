class ShowsController < ApplicationController
  def index
    @user = (params[:user_id] && User.find(params[:user_id])) || current_user
    @next_shows = @user.shows.to_watch
    @old_shows = @user.shows.before_now
    render layout: !request.xhr?
  end

  def new
    @show = Show.new
  end

  def create
    @show = current_user.shows.build(show_params)
    if @show.save
      redirect_to @show, notice: I18n.t("messages.show_created")
    else
      render :new
    end
  end

  def show
    find_show    
  end

  private
  def show_params
    params.require(:show).permit(:name, :media, :show_time)
  end

  def find_show
    @show = Show.find(params[:id])
  end
end
