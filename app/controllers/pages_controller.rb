class PagesController < ApplicationController
  def index
    @today_shows = current_user.shows.today_shows
  end
end
