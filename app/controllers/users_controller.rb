class UsersController < ApplicationController
  skip_before_action :authorize, only: [ :new, :create ]
  
  def index
    @users = User.others(current_user.id)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_path, notice: I18n.t("messages.successfully_signed_up")
    else
      render :new
    end
  end

  def show
    find_user
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def find_user
    @user = User.find(params[:id])
  end
end
