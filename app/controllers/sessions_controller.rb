class SessionsController < ApplicationController
  skip_before_action :authorize
  def new
  end

  def create
    user = User.where(:email => params[:login][:email]).first

    if user && user.authenticate(params[:login][:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: I18n.t("messages.successfully_login")
    else
      flash.now[:error] = I18n.t("messages.login_error")
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: I18n.t("messages.successfully_logout")
  end
end
