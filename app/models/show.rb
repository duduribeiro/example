class Show < ActiveRecord::Base
  belongs_to :user
  validates :name, presence: true
  validates :media, presence: true
  validates :user, presence: true
  validates_datetime :show_time, allow_blank: true

  scope :to_watch, lambda { where(["show_time >= ?", Time.now]).order("show_time ASC") }
  scope :before_now, lambda { where(["show_time < ? OR show_time IS NULL", Time.now]) }
  scope :today_shows, lambda { where(["show_time BETWEEN ? AND ?", DateTime.now.beginning_of_day, DateTime.now.end_of_day]) }

  enum media: { tv: "tv", series: "series", movie: "movie" }

  def media_text
    I18n.t("activerecord.attributes.show.medias.#{media}")
  end
  def self.media_attributes_for_select
    media.map do |media, _|
      [I18n.t("activerecord.attributes.#{model_name.i18n_key}.medias.#{media}"), media]
    end
  end  
end
