class User < ActiveRecord::Base
  has_secure_password
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  has_many :shows, dependent: :destroy

  scope :others, ->(id) { where.not(:id => id)}
end
