module ApplicationHelper
  @@flash_messages = ActiveSupport::HashWithIndifferentAccess.new(
    success: "alert-success", error: "alert-danger",
    alert: "alert-block", notice: "alert-success"
  )
  def error_tag(model, attribute)
    if model.errors.has_key? attribute
      content_tag(
        :div,
        model.errors[attribute].first,
          class: 'error_message'
      )
    end
  end

  def cancel_link(path)
    link_to I18n.t("menu.cancel_button"), path, class: "btn btn-danger", data: { confirm: I18n.t("messages.cancel_operation")  }
  end  

  def error_explanation(model)
    if model.errors.any?
      content_tag :div, id: "error_explanation", class: "alert alert-danger" do
        I18n.t("messages.error_explanation")
      end
    end
  end

  def bootstrap_class_for(flash_type)
    @@flash_messages[flash_type] || flash_type.to_s
  end  

end
