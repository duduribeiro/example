class BootstrapFormBuilder < ActionView::Helpers::FormBuilder
  %w[text_field text_area password_field email_field collection_select].each do |method_name|
    define_method(method_name) do |name, *args|
      options = args.extract_options!
      options[:class] =  "form-control"
      options[:placeholder] ||= self.object.class.human_attribute_name(name) if self.object.class.respond_to?("human_attribute_name")

      @template.content_tag :div, class: "form-group" do
        label(options[:label] || name) + super(name, options) + error_text(name)
      end
    end
  end

  def label(name)
    super(name, class: "control-label")
  end

  def select(method, choices = nil, options = {}, html_options = {}, &block)
    html_options[:class] ||= "form-control"
    @template.content_tag :div, class: "form-group" do
      label(method) + super(method, choices, options, html_options, &block) + error_text(method)
    end    
  end

  def submit_and_cancel(cancel_link, *args)
    @template.content_tag :div, class: "form-group" do
      @template.content_tag :div, class: "btn-toolbar" do
        submit(args, class: "btn btn-primary") +  cancel_link
      end
    end
  end

  private
  def error_text(attribute)
    object = @template.instance_variable_get("@#{@object_name}")
    error_text = ""
    unless object.nil?
      errors = object.errors[attribute]
      if errors
        "<label class=\"control-label has-error\">#{errors.is_a?(Array) ? errors.first : errors}</label>".html_safe
      else
        ""
      end
    end
  end
end