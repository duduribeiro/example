module ShowDecorator
  def link_to_show
    link = link_to name, show_path(self)
    link += if show_time.present?
      " - #{l(show_time, format: :short)}"
    end
  end
end